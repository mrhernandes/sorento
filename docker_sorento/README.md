# Instruções para iniciar o conteiner

* Use o comando a seguir para iniciar um conteiner do Sorento usando HTTP e
se conectar a um servidor Zabbix (também com HTTP).

```sh
VERSAO=0.0.1

docker run -d -p 8080:80 \
--name sorento \
--restart=always \
-e URL_ZABBIX="http://192.168.0.1/zabbix" \
-e LOGIN_ZABBIX="myuser" \
-e PASSWD_ZABBIX="mypass" \
zabbixbr/sorento:$VERSAO
```

Acesse o Sorento através da URL http://IP-SERVER:8080

Observação: Por enquanto o Sorento ainda não funciona em HTTPS e não dá suporte
a acessar o Zabbix com HTTPS.

* Se precisar visualizar o log do conteiner, use o seguinte comando.

```sh
docker logs -f sorento
```

* Se precisar parar/iniciar o conteiner, use o seguinte comando.

```sh
docker stop sorento
docker start sorento
```

* Se precisar remover o conteiner, use o seguinte comando.

```sh
docker rm -f sorento
```

# Instruções para baixar e compilar a imagem Docker do Sorento

Observação: Os passos a seguir devem ser executados apenas pela equipe de
desenvolvimento do Sorento.

* Baixe o código do repositório Git com o comando a seguir.

```sh
git clone https://gitlab.com/mrhernandes/sorento.git
```

* Altere o arquivo ``Dockerfile`` sempre que houver alterações nas dependências
de software ou nos arquivos de configuração.

* Gere uma nova versão da imagem Docker

```sh
cd sorento/
VERSAO=0.0.1
docker build -f docker_sorento/Dockerfile --build-arg APP_VERSION=$VERSAO -t zabbixbr/sorento:$VERSAO .
```

* Envie a imagem para Docker Hub com os seguintes comandos.

```sh
docker login -u SEU_USUARIO_DOCKER_HUB -p SUA_SENHA_DOCKER_HUB
docker tag zabbixbr/sorento:$VERSAO zabbixbr/sorento
docker push zabbixbr/sorento:$VERSAO
docker push zabbixbr/sorento
```
