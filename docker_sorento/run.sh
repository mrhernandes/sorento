#!/bin/bash
# Alterando as credenciais de acesso ao Zabbix no Sorento

#------------------
# VARIAVEIS
#------------------
URL_ZBX=$1
LOGIN_ZBX=$2
PASSWD_ZBX=$3
FILE_CONFIG=/var/www/html/config.inc.php

#------------------
# MAIN
#------------------

sed -i.old "s~^\$zbx_server = 'http://you_ip_address/zabbix/api_jsonrpc.php';~\$zbx_server='$URL_ZBX';~; \
            s~^\$zbx_user  = 'user';~\$zbx_server='$LOGIN_ZBX';~; \
            s~^\$zbx_pass = 'your_password';~\$zbx_server='$PASSWD_ZBX';~ \
            g" $FILE_CONFIG;
