<?php
//This is a config file 
//Configure parameters for your zabbix server info

//Zabbix Server Option
//The Zabbix Server or Zabbix Frontend address with api details.
//Mandatory: yes
//Example:
//$zbx_server = 'http://localhost/zabbix/api_jsonrpc.php';
$zbx_server = 'http://you_ip_address/zabbix/api_jsonrpc.php';

//Zabbix User Option
//This zabbix user with access read only to enviroment monitor.
//Mandatory: yes
//$zbx_user  = 'zabbix_user ';
$zbx_user  = 'user';

//Zabbix Password Option
//Zabbix user password case sensitive.
//Mandatory: yes
//$zbx_pass = 'User_Password';
$zbx_pass = 'your_password';


?>
