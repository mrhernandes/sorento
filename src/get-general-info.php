<?php
include ('../inc/header.html');
include ('../config.inc.php');

// load ZabbixApi
require_once ("../PhpZabbixApi/build/ZabbixApi.class.php");

use ZabbixApi\ZabbixApi;
try
{	
	// connect to Zabbix API
	$api = new ZabbixApi($zbx_server, $zbx_user, $zbx_pass);
	?>

	<body>
	<h1>Relatorio Geral do Zabbix</h1>
	<h3>
	<?php
	echo "Data: ". date("d-m-Y h:i:sa");
	?>
	<p>Area:</p>
	</h3>
	</body>

	<?php

	//Print total de grupos existentes	
	$ttl_groups = $api->hostgroupGet(array(
				'output'=>array(
					'name','groupid')
				));
	
	echo "Total de grupos existentes: ",count($ttl_groups),"<br/>\n";

	//Print total de templates existentes
	$ttl_templates =$api->templateGet(array(
				'output'=>array(
					'templateid','name')
				));
	
	echo "\nTotal de templates existentes: ",count($ttl_templates),"<br/>\n";

	//Print total de hosts cadastrados
	$ttl_hosts_cadastrados = $api->hostGet(array(
				'output'=>array(
					'hostid','name','status')
				));
	
	echo "\nTotal de hosts cadastrados: ",count($ttl_hosts_cadastrados),"<br/>\n";

	//Print total de hosts habilitados
	$ttl_hosts_habilitados = $api->hostGet(array(
				'output'=>array(
					'hostid','name','status'),
					'filter'=> array(
						'status'=> '0')
				));

	echo "\nTotal de hosts habilitados: ",count($ttl_hosts_habilitados),"<br/>\n";

	//Print total de hosts desabilitados
	$ttl_hosts_desabilitados = $api->hostGet(array(
				'output'=>array(
					'hostid','name','status'),
					'filter'=> array(
						'status'=> '1')
				));

	echo "\nTotal de hosts desabilitados: ",count($ttl_hosts_desabilitados),"<br/>\n";

	//	var_dump($hosts);
	//Print total de itens nao suportados
	$items = $api->itemGet(array(
                                'output'=>array(
                                         'state'),
   
                                'filter'=>array(
                                           'state'=>'1'
                                          ),
                                  )
                          );
       echo "Total de itens nao suportados: ";
       echo count($items);
       echo "</br>\n";

echo "</br>\n";
include'../inc/footer.html';

echo "</br>\n";
include '../inc/printer.php';

}
catch(Exception $e)
{
// Exception in ZabbixApi catched
	 echo $e->getMessage();
}
// Logout da API do Zabbix
$api->userLogout([])
?>
