<?php
include ('../inc/header.html');
include ('../config.inc.php');

// load ZabbixApi
require_once ("../PhpZabbixApi/build/ZabbixApi.class.php");

use ZabbixApi\ZabbixApi;
try
{
	// connect to Zabbix API
	$api = new ZabbixApi($zbx_server, $zbx_user, $zbx_pass);
	
	?>	

	<body>
        <h1>Relatorio de items nao suportados do Zabbix</h1>
        	<h3>
        			<?php
			        	echo "Data: ". date("d-m-Y h:i:sa");
        			?>
        		<p>Area: Projetos</p>
        	</h3>
        </body>

	<table border='1' class='stats' cellspacing='0'>

	<tr>
	<center>
            <th>HostID</th>
            <th>Hostname</th>
            <th>Item name</th>
            <th>State</th>
	</center>
	</tr>	
	
<?php
     //Total de itens nao suportados
     $items = $api->itemGet(array(
                             'output'=>array(
                                         'state'),

                              'filter'=>array(
                                         'state'=>'1'
                                        ),
                                 )
                         );
     echo "Total de itens nao suportados: ";
     echo count($items);
     echo "</br>\n";
     echo "</br>\n";
?>


	<?php
	$hosts = $api->hostGet(array(
				'output'=>array(
					'name','hostid')
				)
			);
	$items = $api->itemGet(array(
				'output'=>array(
					'hostid','name','state','key_','itemid'),
				
				'filter'=>array(
					'state'=>'1'
					)					
				)
			);
//	var_dump($hosts);

	foreach($hosts as $host)
		foreach ($items as $item) 
			//if ($item->hostid == $host->hostid) 
			if ($host->hostid == $item->hostid) 
			{
			echo "<tr>
				 <td>" .("$host->hostid") ."</td>
				 <td>" .("$host->name") ."</td>
				<td>" .("$item->name") ."</td>
				<td>" .("$item->state") ."</td>
			  
	    		  	 </tr>";
			}
	echo "</table>";
	
echo "</br>\n";
include ('../inc/footer.html');

echo "</br>\n";
include ('../inc/printer.php');
}
catch(Exception $e)
{
// Exception in ZabbixApi catched
	 echo $e->getMessage();

}
// Logout da API do Zabbix
$api->userLogout([])

?>
