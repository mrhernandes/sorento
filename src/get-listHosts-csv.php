<?php
// load ZabbixApi
require_once 'PhpZabbixApi/build/ZabbixApi.class.php';
header('Content-Type: text/plain; charset=utf-8');

use ZabbixApi\ZabbixApi;
try
{
	// connect to Zabbix API
 	$api = new ZabbixApi('http://192.168.56.3/zabbix/api_jsonrpc.php', 'Admin', 'zabbix');
	
//	$api->setDefaultParams(array('output' => 'extend'));

//	$hosts = $api->hostGet();
//	$graphs = $api->graphGet();
	
	$hosts = $api->hostGet(array(
				'output'=>array(
					'name')
				)
			);
	
	
//	var_dump($hosts);
	echo "Relatorio de Hosts Zabbix \n";
	echo "\n";

	foreach($hosts as $host)
		printf ("$host->name\n");	

}
catch(Exception $e)
{
// Exception in ZabbixApi catched
	 echo $e->getMessage();
}
// Logout da API do Zabbix
$api->userLogout([])
?>
