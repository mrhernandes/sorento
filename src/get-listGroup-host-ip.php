<?php
include ('../inc/header.html');
include ('../config.inc.php');

// load ZabbixApi
require_once ("../PhpZabbixApi/build/ZabbixApi.class.php");

use ZabbixApi\ZabbixApi;
try
{
	// connect to Zabbix API
	$api = new ZabbixApi($zbx_server, $zbx_user, $zbx_pass);	
	?>
        <body>
        	<h1>Relatorio de Grupos + Hostaname + IP do Zabbix</h1>
        		<h3>
        			<?php
        				echo "Data: ". date("d-m-Y h:i:sa");
        			?>
        			<p>Area:</p>
        		</h3>
        </body>
	
	<?php
	//get hostgroup list 
	$hostgroups = $api->hostgroupGet(array(
					'output'=>array(
						'name','groupid')	
					)
				);
//	var_dump($hosts);

	foreach($hostgroups as $group)
		printf ("$group->name</br>\n");	


echo "</br>\n";
include ('../inc/footer.html');

echo "</br>\n";
include ('../inc/printer.php');

}
catch(Exception $e)
{
// Exception in ZabbixApi catched
	 echo $e->getMessage();
}
// Logout da API do Zabbix
$api->userLogout([])
?>
