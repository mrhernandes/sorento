<?php
include ('../inc/header.html');
include ('../config.inc.php');

// load ZabbixApi
require_once ("../PhpZabbixApi/build/ZabbixApi.class.php");

use ZabbixApi\ZabbixApi;
try
{
	// connect to Zabbix API
	$api = new ZabbixApi($zbx_server, $zbx_user, $zbx_pass);	
	?>
        <body>
        	<h1>Relatorio de Hosts + IP do Zabbix</h1>
        		<h3>
        			<?php
        				echo "Data: ". date("d-m-Y h:i:sa");
        			?>
        			<p>Area:</p>
        		</h3>
        </body>
	
	<?php
	//get list host 	
	$hosts = $api->hostGet(array(
				'output'=>array(
					'name','hostid')
				)
			);

	//get list interfaces ip
	$interfaces = $api->hostinterfaceGet(array(
				'output'=>array(
					'hostid','ip')
			)
	);


//	var_dump($hosts);

	foreach($hosts as $host)
		foreach($interfaces as $iface)
			if ($host->hostid == $iface->hostid)
			
			printf ("$host->name;$iface->ip</br>\n");	
echo "</br>\n";
include ('../inc/footer.html');

echo "</br>\n";
include ('../inc/printer.php');

}
catch(Exception $e)
{
// Exception in ZabbixApi catched
	 echo $e->getMessage();
}
// Logout da API do Zabbix
$api->userLogout([])
?>
