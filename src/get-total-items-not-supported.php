<?php
    #==============================================================================
    #@file get-problems.php
    #This file is part of scripts Sorento Zabbix Report
    #@author Hernandes Martins
    #@email hernandss@gmail.com
    #@community Zabbix Brasil
    #@site zabbixbrasil.org
    #@documentation https://gitlab.com/mrhernandes/sorento/
    #@copyright   GNU General Public License
    #=============================================================================

    #==============================================================================
    #Set includes
    #==============================================================================
    include ('../inc/header.html');
    include ('../config.inc.php');
    ?>

    <!DOCTYPE html>
    <html>
    <body>
        <h1>Total de itens nao suportados do Zabbix</h1>
            <h3>
                <?php echo "Data: ". date("d-m-Y h:i:sa");?>
                <p>Area: Projetos</p>
            </h3>
    </body>
    </html>

    <?php
    #==============================================================================
    # PHP Zabbix API
    #==============================================================================
    require_once ("../PhpZabbixApi/build/ZabbixApi.class.php");

    #==============================================================================
    #Load ZabbixApi
    #==============================================================================
    use ZabbixApi\ZabbixApi;

    #==============================================================================
    #Connect to Zabbix API
    #==============================================================================
    try
    {
    $api = new ZabbixApi($zbx_server, $zbx_user, $zbx_pass);

    #==============================================================================
    #get trigger problem events
    #============================================================================== 
    $items = $api->itemGet(array(
			     'output'=>array(
					'state'),
				
			     'filter'=>array(
					'state'=>'1'
					),					
				)
			);
    echo "Total de itens nao suportados: "; 
    echo count($items);
    echo "</br>\n";
   
    #==============================================================================
    #Footer Default Report
    #============================================================================== 
    echo "</br>\n";
    include ('../inc/footer.html');
    
    #==============================================================================
    #Footer Default Report
    #============================================================================== 
    echo "</br>\n";
    include ('../inc/printer.php');
    }
    
    #==============================================================================
    #Exception in ZabbixApi catched
    #============================================================================== 
    catch(Exception $e)
    {
      echo $e->getMessage();
    }
    
    #==============================================================================
    #Logout API Zabbix
    #============================================================================== 
    $api->userLogout()
?>
