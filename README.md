# sorento #
![](images/sorento-print.png)

[Português]: #português
[Requisitos]: #requisitos
[Instruções]: #instruções-de-uso
[English]: #english
[Requirements]: #requirements
[Instructions]: #instructions
[Developers]: #developers
[License]: #license

#### Conteúdo

1. [Português][Português]
    - [Requisitos][Requisitos]
    - [Instruções de uso][Instruções]
2. [English][English]
    - [Requirements][Requirements]
    - [Instructions][Instructions]
3. [Developers][Developers]
4. [License][License]

# Português

Este é o repositório do Sorento, uma interface web para consulta e exportação
de relatórios com dados cadastrados no Zabbix.

O Sorento é desenvolvido em PHP e usa a biblioteca ``PhpZabbixApi``
https://github.com/confirm/PhpZabbixApi.

## Requisitos

1. Ter um Zabbix instalado e uma conta de acesso com permissão de leitura à API (Application Programming Interface).

## Instruções de Uso

* Baixe os códigos com o comando a seguir.

```sh
git clone https://gitlab.com/mrhernandes/sorento.git
cd sorento
```

* Execute os comandos da página [README](docker_compose/README.md) para iniciar
o conteiner Docker da aplicação.

# English

This is the Sorento repository, a web interface for query and export of reports
with data registered in Zabbix.

Sorento is developed in PHP and uses the ``PhpZabbixApi`` library
https://github.com/confirm/PhpZabbixApi.

## Requirements

1. Have a Zabbix installed and an access account with permission of
to the API (Application Programming Interface).

## Instructions

* Download the codes with the following command.

```sh
git clone https://gitlab.com/mrhernandes/sorento.git
cd sorento
```

* Execute commands from the [README](docker_compose/README.md) page to start
the application's Docker container.

## Developers

Aécio dos Santos Pires

Site: http://aeciopires.com

Hernandes Martins

Site: http://hernandesmartins.blogspot.com

## Licence

MIT License
